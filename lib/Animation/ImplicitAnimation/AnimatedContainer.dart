import 'package:flutter/material.dart';

class ClassAnimatedContainer extends StatefulWidget {
  @override
  _ClassAnimatedContainerState createState() => _ClassAnimatedContainerState();
}

class _ClassAnimatedContainerState extends State<ClassAnimatedContainer> {
  Color _color = Colors.blue;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Animated Container'),
      ),
      body: Row(
        children: [
          AnimatedContainer(
            height: 100,
            width: 100,
            margin: EdgeInsets.all(20),
            duration: Duration(seconds: 2),
            color: _color,
          ),
          RaisedButton(
            child: Text('Click me to change color'),
            onPressed: (){
              setState(() {
                _color = Colors.pink;
              });
            },
          ),
        ],
      ),
    );
  }
}
