import 'package:flutter/material.dart';

class ListViewBuilderClass extends StatefulWidget {
  @override
  _ListViewBuilderClassState createState() => _ListViewBuilderClassState();
}

class _ListViewBuilderClassState extends State<ListViewBuilderClass> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (context, indexabc) => [
          SliverAppBar(
              leading: IconButton(
            icon: Icon(Icons.menu),
            iconSize: 20.0,
          ))
        ],
        body: ListView(children: [
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
          Text('1'),
          SizedBox(
            height: 20,
          ),
        ]),
      ),
    );
  }
}
