import 'package:flutter/material.dart';

class ButtonAnimation extends StatefulWidget {
  @override
  _ButtonAnimationState createState() => _ButtonAnimationState();
}

class _ButtonAnimationState extends State<ButtonAnimation> {
  bool isLoading = false;
  bool isStretch = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Button Animation'),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(20),
        child: isStretch ? ElevatedButton(
          onPressed: () async{
            setState(() {
              isStretch = false;
            });
            await Future.delayed(Duration(seconds: 2));
            setState(() {
              isStretch = true;
            });
          },
          child: Text('SUBMIT',),
          style: ElevatedButton.styleFrom(
           // minimumSize: Size.fromHeight(60),
            primary: Colors.pink,
            shape: StadiumBorder(),
            textStyle: TextStyle(
              color: Colors.orange,
              fontSize: 20
            )
          ),
        ) :
        Container(
          width: 100,
          height: 50,
          decoration: BoxDecoration(
            shape: BoxShape.circle,color: Colors.indigo
          ),
          child: CircularProgressIndicator(color: Colors.white,),
        )

      ),
    );
  }
}
/* Simple ElevatedButton
import 'package:flutter/material.dart';

class ButtonAnimation extends StatefulWidget {
  @override
  _ButtonAnimationState createState() => _ButtonAnimationState();
}

class _ButtonAnimationState extends State<ButtonAnimation> {
  bool isLoading = false;
  //bool isStretch = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Button Animation'),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(20),
        child: ElevatedButton(
          onPressed: () async{
            setState(() {
              isLoading = true;
            });
            await Future.delayed(Duration(seconds: 2));
            setState(() {
              isLoading = false;
            });
          },
          child: isLoading ? CircularProgressIndicator(color: Colors.white,) :
          Text('SUBMIT',),
          style: ElevatedButton.styleFrom(
           // minimumSize: Size.fromHeight(60),
            primary: Colors.pink,
            shape: StadiumBorder(),
            textStyle: TextStyle(
              color: Colors.orange,
              fontSize: 20
            )
          ),
        ),

      ),
    );
  }

  Widget BuildButton() => OutlinedButton(
      onPressed: (){},
      style: ElevatedButton.styleFrom(
        // minimumSize: Size.fromHeight(60),
          primary: Colors.pink,
          shape: StadiumBorder(),
          textStyle: TextStyle(
              color: Colors.orange,
              fontSize: 20
          )
      ),
      child:  Text('SUBMIT')
  );
}

 */
