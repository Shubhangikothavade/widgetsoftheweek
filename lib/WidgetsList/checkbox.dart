import 'package:flutter/material.dart';
import 'package:widgetsoftheweek_demo/ClassLibrary/notification_setting.dart';

class ClassCheckBox extends StatefulWidget {
  @override
  _ClassCheckBoxState createState() => _ClassCheckBoxState();
}

class _ClassCheckBoxState extends State<ClassCheckBox> {
  bool value = false;
  final notifications = [
    NotificationSetting(title: 'West Des Moines'),
    NotificationSetting(title: 'Chicago, IL'),
    NotificationSetting(title: 'Phoenix, AZ'),
    NotificationSetting(title: 'Dallas, TX'),
    NotificationSetting(title: 'San Diego, CA'),
    NotificationSetting(title: 'San Fransisco, CA'),
    NotificationSetting(title: 'New York, ZK'),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //NotificationSetting()
  }
  @override
  Widget build(BuildContext context) {
    return  Column(
        children: [
          ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              ...notifications.map(buildSingleCheckbox).toList(),
            ],
          ),

        ],
      );
  }

  Widget buildSingleCheckbox(NotificationSetting notification) {
    return buildCheckBox(
      notification: notification,
      onClicked: () {
        setState(() {
          final newValue = !notification.value;
          notification.value = newValue;
        });
      },);
  }

  Widget buildCheckBox({ @required NotificationSetting notification,
    @required VoidCallback onClicked,
  })
  {
    return ListTile(
      onTap: onClicked,
      leading: Checkbox(
        value: notification.value,
        onChanged: (value) => onClicked(),
      ),
      title: Text(notification.title,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal),),
    );
  }
}
