import 'package:flutter/material.dart';

import 'checkbox.dart';

class VerticleTab extends StatefulWidget {
  @override
  _VerticleTabState createState() => _VerticleTabState();
}

class _VerticleTabState extends State<VerticleTab> {
  int _selectedIndex = 0;
  PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        //title: Text('Vertcle Tab for filter implementation'),
        leading: Text('Filters'),
        actions: [
          Text('Clear All'),
        ],
      ),
      body: buildVerticleTab(),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Text('Close'), Text('Apply')],
          ),
        ),
      ),
    );
  }

  Widget buildVerticleTab() => Row(
        children: [
          SizedBox(
            width: 100,
            child: ListView.separated(
                //shrinkWrap: true,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedIndex = index;
                        _pageController.jumpToPage(index);
                      });
                    },
                    child: Container(
                      child: Row(
                        children: [
                          // AnimatedContainer(duration: Duration(microseconds: 500),
                          // color: Colors.blue,
                          // height: _selectedIndex == index ? 20 : 0,
                          // width: 5,
                          // ),
                          Expanded(
                            child: Container(
                                color: _selectedIndex == index
                                    ? Colors.grey
                                    : Colors.transparent,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('FilterTab : $index'),
                                )),
                          )
                        ],
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, int) {
                  return SizedBox(
                    height: 20,
                  );
                },
                itemCount: 3),
          ),
          Expanded(
              child: PageView(
            controller: _pageController,
            children: [
              ClassCheckBox(),
              Container(
                child: Text('tab 1'),
              ),
              Container(
                child: Text('tab 2'),
              )
            ],
          ))
        ],
      );
}
