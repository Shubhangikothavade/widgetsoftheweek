import 'package:flutter/material.dart';
import 'package:widgetsoftheweek_demo/WidgetsList/checkbox.dart';
import 'Animation/ExplicitAnimation/AnimatedBuilderExample.dart';
import 'Animation/ImplicitAnimation/ListAnimation.dart';
import 'Animation/ImplicitAnimation/TweenAnimationBuilder.dart';
import 'Screens/login_page.dart';
import 'WidgetsList/BottomAppBar.dart';
import 'WidgetsList/ListViewBuilder.dart';
import 'WidgetsList/button_animation.dart';
import 'WidgetsList/verticletab.dart';
import 'file:///C:/Users/Lenovo/AndroidStudioProjects/widgetsoftheweek_demo/lib/Animation/ImplicitAnimation/AnimatedContainer.dart';
void main() {
  //runApp(MyApp());
  runApp(MaterialApp(
    // Shimmer effect :-)

    //title: 'RowColumn',
    //home: RowColumn(),

    // Show n Hide AppBar
    //title: 'ListViewBuilder',
    //home: ListViewBuilderClass(),

    //title: 'Implicit Animation',
    //home: ClassAnimatedContainer(),

    //title: 'Implicit Animation',
    //home: ClassTweenAnimationBuilder(),

    //title: 'Explicit Animation',
    //home: ClassExplicitAnimation(),

    //title: 'Explicit Animation',
    //home: ClassCheckBox(), //ClassListAnimation(),

     title: 'FilterList',
     home: VerticleTab(),

      // title: 'Submit Button',
      // home: ButtonAnimation(),
     // title: 'Submit Button',
     // home: LoginPage(),

  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Widgets Of The Week..'),
        ),
        body: Stack(
          alignment: Alignment.center,
          clipBehavior: Clip.none,
          //overflow: Overflow.visible,
          children: [
            Container(
              height: 300,
              width: 300,
              color: Colors.blue,
            ),
            Positioned(
                height: 200,
                width: 200,
                top: 130,
                child: Container(
                  color: Colors.pink,
                )),
            Positioned(
                height: 100,
                width: 100,
                bottom: -50,
                child: Container(
                  color: Colors.orange,
                )),
          ],
        ),
      ),
    );
  }
}
